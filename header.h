#ifndef HEADER_H
#define HEADER_H

typedef struct datum
{
	int yyyy;
	int mm;
	int dd;
}DATUM;

typedef struct pivovara
{
	unsigned int idPiva;
	float cijena;
	char imePiva[30];
	char okusPiva[30];
	DATUM istekRokaPiva;
}PIVO;

void porukaIzbornik(const char*);
void dodavanjeInformacija();
void selectionSortCijenaUzlazno(PIVO*, int);
void selectionSortCijenaSilazno(PIVO*, int);
void selectionSortIDuzlazno(PIVO*, int);
void selectionSortIDsilazno(PIVO*, int);
void selectionSortImeSilazno(PIVO*, int);
void selectionSortImeUzlazno(PIVO*, int);
void sortiranje(PIVO*, int);
void uredjivanjeInformacija();
void sortiranjeIzbornik(int);
void pretrazivanje();
int brojacPiva();
void ispisInformacija();
void brisanjeInformacija();
void izlazak(void);
void izbornik();

#endif // !HEADER_H